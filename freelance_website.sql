-- phpMyAdmin SQL Dump
-- version 4.4.15.5
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Apr 05, 2017 at 08:12 PM
-- Server version: 5.5.49-log
-- PHP Version: 7.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `freelance_website`
--

-- --------------------------------------------------------

--
-- Table structure for table `candidates`
--

CREATE TABLE IF NOT EXISTS `candidates` (
  `id` int(10) unsigned NOT NULL,
  `id_freelance` int(11) NOT NULL,
  `id_project` int(11) NOT NULL,
  `test_note` int(11) NOT NULL,
  `comment` varchar(250) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(10) unsigned NOT NULL,
  `dt_create` datetime NOT NULL,
  `id_sender` int(11) NOT NULL,
  `id_project` int(11) NOT NULL,
  `message` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `object` varchar(250) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `freelances`
--

CREATE TABLE IF NOT EXISTS `freelances` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(11) NOT NULL,
  `firstname` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `date_birthday` datetime NOT NULL,
  `address` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `tel` int(11) NOT NULL,
  `spe` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `comment` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `note` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `freelances`
--

INSERT INTO `freelances` (`id`, `user_id`, `firstname`, `lastname`, `date_birthday`, `address`, `country`, `city`, `tel`, `spe`, `comment`, `remember_token`, `created_at`, `updated_at`, `note`) VALUES
(1, 1, 'joeyyyy', 'joej', '2017-03-01 00:00:00', 'aadrres', 'coutnyr', 'city', 654645, '1', ',ln', NULL, '2016-12-16 08:48:33', '2017-03-22 20:09:37', 1),
(2, 3, 'pjkj', 'lkjlkj', '2017-01-18 00:00:00', 'kjlkj', 'kjljh', 'lkjljk', 354354354, '1', 'dcdf', NULL, '2017-01-04 20:41:50', '2017-01-04 20:41:50', NULL),
(3, 4, 'ljklkj', 'lkjlk', '2017-01-10 00:00:00', 'dsffd', 'zfdsf', 'dsfdfsdf', 65468, '1', 'kkk', NULL, '2017-01-04 20:45:14', '2017-01-04 20:45:14', NULL),
(4, 5, 'lkjlkj', 'lkjk', '2017-01-25 00:00:00', 'lkjlkj', 'lkjlkj', 'lkjlkj', 654654, '1', 'kjlkj', NULL, '2017-01-04 20:47:33', '2017-01-04 20:47:33', NULL),
(5, 6, 'kjlkj', 'lkjlkj', '2017-01-10 00:00:00', 'lkjlkj', 'lkjlkj', 'lkjlkj', 654654, '1', 'knjlkj', NULL, '2017-01-04 20:52:26', '2017-01-04 20:52:26', NULL),
(6, 8, 'jojo', 'bonnet', '2017-03-23 00:00:00', 'kjlkj', 'France', 'City', 255887799, '2', 'comment', NULL, '2017-03-18 12:00:25', '2017-03-18 12:00:25', NULL),
(7, 10, 'jojo', 'bonnet', '2017-03-01 00:00:00', 'kjlkj', 'France', 'City', 255887799, '1', 'ffd', NULL, '2017-03-31 07:36:00', '2017-03-31 07:36:34', 1);

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(10) unsigned NOT NULL,
  `dt_create` datetime NOT NULL,
  `id_sender` int(11) NOT NULL,
  `id_receiver` int(11) NOT NULL,
  `message` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `object` varchar(250) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(61, '2014_10_12_000000_create_users_table', 1),
(62, '2014_10_12_100000_create_password_resets_table', 1),
(63, '2016_09_29_205920_message', 1),
(64, '2016_09_29_210622_comments', 1),
(65, '2016_09_29_210737_users', 1),
(66, '2016_09_29_211205_freelance', 1),
(67, '2016_09_29_214228_society', 1),
(68, '2016_09_29_214602_project', 1),
(69, '2016_09_29_214602_response', 1),
(70, '2016_09_29_215608_test', 1),
(71, '2016_09_29_215842_candidate', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE IF NOT EXISTS `projects` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `spe` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `society_id` int(11) NOT NULL,
  `id_freelance` int(11) DEFAULT NULL,
  `description` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `statut` int(11) NOT NULL DEFAULT '0',
  `dt_start` datetime NOT NULL,
  `dt_end` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `name`, `spe`, `society_id`, `id_freelance`, `description`, `price`, `statut`, `dt_start`, `dt_end`, `created_at`, `updated_at`) VALUES
(1, 'Projet1', 'info', 1, NULL, 'un projet ablalbla', 133, 0, '2017-03-01 00:00:00', '2017-03-16 00:00:00', '2017-02-28 23:00:00', NULL),
(2, 'projet', 'espe', 1, NULL, 'esprit', 133, 0, '2017-03-03 00:00:00', '2017-03-24 00:00:00', '2017-03-18 12:18:50', '2017-03-18 12:18:50'),
(3, 'ououou', 'kjhkjh', 3, NULL, 'kjhkjhkjh', 165, 0, '1998-02-13 00:00:00', '2000-10-16 00:00:00', '2017-03-18 13:09:09', '2017-03-18 13:09:09'),
(4, 'un projet', 'lkjlkj', 2, NULL, 'kjlj', 544545, 0, '2017-03-02 00:00:00', '2017-03-04 00:00:00', '2017-03-20 21:18:28', '2017-03-20 21:18:28'),
(5, 'Avion', 'la spe', 2, 1, 'une descripiton', 5000, 0, '2017-04-05 00:00:00', '2017-04-15 00:00:00', '2017-04-03 16:31:00', '2017-04-03 16:39:42');

-- --------------------------------------------------------

--
-- Table structure for table `response`
--

CREATE TABLE IF NOT EXISTS `response` (
  `id` int(10) unsigned NOT NULL,
  `id_question` int(11) NOT NULL,
  `response` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `isTrue` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `response`
--

INSERT INTO `response` (`id`, `id_question`, `response`, `isTrue`) VALUES
(1, 1, '1', 0),
(2, 1, '2', 1),
(3, 1, '3', 0),
(4, 1, '4', 0),
(5, 2, 'a', 0),
(6, 2, 's', 0),
(7, 2, 'zz', 1),
(8, 2, 'b', 0),
(9, 3, 'lou', 1),
(10, 3, 'kik', 0),
(11, 3, 'juju', 0),
(12, 3, 're', 0),
(13, 4, 'ah ?', 0),
(14, 4, 'be ?', 1),
(15, 4, 'ce ?', 0),
(16, 4, 'De ?', 0);

-- --------------------------------------------------------

--
-- Table structure for table `societies`
--

CREATE TABLE IF NOT EXISTS `societies` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `code_postal` int(11) NOT NULL,
  `tel` int(11) NOT NULL,
  `spe` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `comment` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `societies`
--

INSERT INTO `societies` (`id`, `user_id`, `name`, `address`, `country`, `city`, `code_postal`, `tel`, `spe`, `comment`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 2, 'name', 'adress', 'coutntr', 'city', 6150, 66666, '2', 'comment', NULL, '2016-12-16 09:40:46', '2016-12-16 09:40:46'),
(2, 1, 'moule', 'lkjlk', 'lkjlkj', 'lkj', 65464, 6455, '1', 'lk,', NULL, '2017-01-04 20:56:55', '2017-01-04 20:56:55'),
(3, 9, 'soci', 'kjhkjh', 'jkhkjh', 'kjhkjh', 6548, 665487589, '1', 'khh', NULL, '2017-03-18 13:08:29', '2017-03-18 13:08:29');

-- --------------------------------------------------------

--
-- Table structure for table `tests`
--

CREATE TABLE IF NOT EXISTS `tests` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `question` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `reponse` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `spe` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tests`
--

INSERT INTO `tests` (`id`, `name`, `question`, `reponse`, `spe`) VALUES
(1, 'Php', 'question php ?', '1-2', 1),
(2, 'SQL', 'le sql ?', '2-3', 2),
(3, 'Freel', 'Do you ?', '3-1', 3),
(4, 'QUestion autre', 'Est ce que ?', '4-14', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL,
  `password` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `password`, `email`, `name`, `type`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, '$2y$10$9M5FUgQMaGsvC7yeR3..IOU0C2q1JjNV.FeVVRx7ueqeSzew8r9Uy', 'joe@mail.fr', 'joeyo', 1, 'si1NPLKUJguAL3tWHYIohKItv2hpjiaXFZ3AMeIMpIoUKSBYom5Ub0UulNe8', '2016-12-16 08:48:33', '2017-03-31 07:34:58'),
(2, '$2y$10$PF9iBmy98oYxWaTdE6ZoR.yKtNbVtJsKvDea0q4zrt1hmma7pnZhy', 'soc@mail.fr', 'soc', 2, 'xU9NAsEeL0bsiqkFR3wUiegU6NgbUMwN8APv7zRODjvBOmYiBJOqmsu4cbNv', '2016-12-16 09:39:42', '2017-03-18 13:07:45'),
(3, '$2y$10$wMTEdgsfq2kLVrdLq.tI6OqHq7Pv5gXgseaXLyFWMkZe6SnESQ9aS', 'mail@joe.fr', 'joe', 1, NULL, '2017-01-04 20:41:50', '2017-01-04 20:41:50'),
(4, '$2y$10$TKkayURVfhWXvRNnCNKkM.A9a5apypOK.ewwV4wTTfX8xNfQQqDsy', 'joe@maill.fr', 'joe', 1, NULL, '2017-01-04 20:45:14', '2017-01-04 20:45:14'),
(5, '$2y$10$ToQIRaaNP3aunHObCKCtx.GiJOXxiCdnU5mO0YIFvr5STRLfU0iMa', 'joe@mail.frsqd', 'fdsfsd', 1, NULL, '2017-01-04 20:47:33', '2017-01-04 20:47:33'),
(6, '$2y$10$nWbjbqpEqo4G9S3jt1TDPOu9N7x3lTDCMKIRsl2aqIht3t0TkNnGC', 'joe@mail.frdd', 'jhkjh', 1, NULL, '2017-01-04 20:52:26', '2017-01-04 20:52:26'),
(7, '$2y$10$V/Ug2wIg8lcVgZv4/zQGN.gOjlbqZURmDqXSIXw3QwhTsa/BjFdoq', 'soc@mail.frr', 'moule', 2, '771KfwJsnhsktFZzNEEQj6K6QqJqvJL9yJqHFtmv0gxv4CStuYr3bMyO5JB0', '2017-01-04 20:56:54', '2017-01-04 20:57:38'),
(8, '$2y$10$FRFRPd0s7/3hSoEVbAxT6.WpZ0/n7697pojyAdtSl.ftgo.uRy4f2', 'bonnet@joe.fr', 'jojo', 1, NULL, '2017-03-18 12:00:24', '2017-03-18 12:00:24'),
(9, '$2y$10$p7IIVtAqNr6XG2Ahsvcs/O9aIetwFO7JhOFFIOrLt.OMR49a/XPwW', 'soc@soc.fr', 'soci', 2, 'HvMVEict6UjTXcj9ERJx1ZIElyS1R8htHUiQP4OSUnnrDy3kZQ4ZwkwkNavl', '2017-03-18 13:08:29', '2017-03-18 13:32:12'),
(10, '$2y$10$XortFCl8bkQ58laDdbnrquMCKCtvgL8f8pD1oWhVujX9wswpBx8Oe', 'mail@mail.test', 'toto', 1, NULL, '2017-03-31 07:36:00', '2017-03-31 07:36:00');

-- --------------------------------------------------------

--
-- Table structure for table `users_old`
--

CREATE TABLE IF NOT EXISTS `users_old` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `candidates`
--
ALTER TABLE `candidates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `freelances`
--
ALTER TABLE `freelances`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `response`
--
ALTER TABLE `response`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `societies`
--
ALTER TABLE `societies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tests`
--
ALTER TABLE `tests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_old`
--
ALTER TABLE `users_old`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_old_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `candidates`
--
ALTER TABLE `candidates`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `freelances`
--
ALTER TABLE `freelances`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=72;
--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `response`
--
ALTER TABLE `response`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `societies`
--
ALTER TABLE `societies`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tests`
--
ALTER TABLE `tests`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `users_old`
--
ALTER TABLE `users_old`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
