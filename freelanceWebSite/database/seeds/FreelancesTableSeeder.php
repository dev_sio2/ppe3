<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use Faker\Factory as Faker;

class FreelancesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker = Faker::create();
    	foreach (range(1, 30) as $index) {
    		DB::table('freelances')->insert([
    		'user_id' => $faker->numberBetween(1,99),
    		'firstname' => $faker->firstname,
            'lastname' => $faker->lastname,
        	'address' => $faker->address,
            'date_birthday' => $faker->dateTime,
        	'city' => $faker->city,
        	'country' => $faker->country,
        	'tel' => $faker->numberBetween(0,99),
        	'spe' => $faker->word,
        	'comment' => $faker->word,
        	'remember_token' => str_random(10),
        	]);
    	}
        
    }
}
