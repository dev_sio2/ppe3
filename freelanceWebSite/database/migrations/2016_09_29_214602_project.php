<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Project extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100);
            $table->string('spe', 100);
            $table->integer('society_id');
            $table->integer('id_freelance')->nullable();
            $table->boolean('postul')->default(false);
            $table->string('statut', 100);
            $table->string('description', 500);
            $table->integer('price');
            $table->datetime('dt_start');
            $table->datetime('dt_end');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('projects');
    }
}
