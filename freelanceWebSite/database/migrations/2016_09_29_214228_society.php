<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Society extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('societies', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('name', 100);
            $table->string('address', 100);
            $table->string('country', 100);
            $table->string('city', 100);
            $table->integer('code_postal');
            $table->string('tel',100);
            $table->string('spe', 100);
            $table->string('comment', 500);
            $table->integer('file_id');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('societies');
    }
}
