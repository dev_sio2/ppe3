@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Modifier votre profil</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('my_profile/update')}}/{{ $user->type }}" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" required autofocus value="{{$user->name}}">
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>
                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" required value="{{$user->email}}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="file">Picture</label>
                            <div class="col-md-9">
                                <input id="hidden" name="hidden" type="hidden" value="{{$user_type->file_id}}">
                                <input id="file" name="file" class="input-file" type="file">
                                @if($user_type->file_id != 0)
                                <img src="{{ url('../storage/app/') }}/{{$user_type->file->filename}}" class="img-responsive" style="    max-width: 100px;">
                                @endif
                            </div>
                        </div>

                        <div>
                            <div class="form-group">
                                <div class="col-md-6 ">
                                <button data-toggle="modal" data-target="#editPasswd" type="button" class="btn  btn-default">
                        Modify password
                        <i class="fa fa-check " aria-hidden="true"></i>
                    </button>
                                </div>
                            </div>
                        </div>

                        @if($user->type == "1")
                            @include('includes.modifFreelance')
                        @else
                            @include('includes.modifSociety')
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@include('includes.editPasswd')
@endsection
