@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Edit Project</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/show_p/update/p/'.$project->id) }}">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{$project->name}}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                            <label for="price" class="col-md-4 control-label">Price</label>

                            <div class="col-md-6">
                                <input id="price" type="number" class="form-control" name="price" value="{{$project->price}}" required>

                                @if ($errors->has('price'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('price') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            <label for="description" class="col-md-4 control-label">Description</label>


                            <div class="col-md-6">
                                <textarea id="description" type="text-area" class="form-control" name="description" required>{{$project->description}}</textarea>

                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                         <div class="form-group{{ $errors->has('spe') ? ' has-error' : '' }}">
                            <label for="spe" class="col-md-4 control-label">Specification</label>

                            <div class="col-md-6">
                                <input id="spe" type="text" class="form-control" name="spe" value="{{$project->spe}}" required>


                                @if ($errors->has('spe'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('spe') }}</strong>

                                    </span>
                                @endif
                            </div>
                        </div>

                      <div class="form-group{{ $errors->has('dt_start') ? ' has-error' : '' }}">
                            <label for="dt_start" class="col-md-4 control-label">Date Start</label>

                            <div class="col-md-6">
                                <input id="dt_start" type="date" class="form-control" name="dt_start" required value="{{$project->dt_start}}">


                                @if ($errors->has('dt_start'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('dt_start') }}</strong>

                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('dt_end') ? ' has-error' : '' }}">
                            <label for="dt_end" class="col-md-4 control-label">Date End</label>

                            <div class="col-md-6">
                                <input id="dt_end" type="date" class="form-control" name="dt_end" required value="{{$project->dt_end}}">


                                @if ($errors->has('dt_end'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('dt_end') }}</strong>

                                    </span>
                                @endif
                            </div>
                        </div> 

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection