<div  class="row freelanceDiv">
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">Edit Freelance Profil</div>
            <div class="panel-body">

                <div class="form-group">
                    <label for="firstname" class="col-md-4 control-label">Firstname</label>
                    <div class="col-md-6">
                        <input id="firstname" type="text" class="form-control" name="firstname" required autofocus value="{{$user_type->firstname}}">
                    </div>
                </div>

                <div class="form-group">
                    <label for="lastname" class="col-md-4 control-label">Lastname</label>
                    <div class="col-md-6">
                        <input id="lastname" type="text" class="form-control" name="lastname" required value="{{$user_type->lastname}}">
                    </div>
                </div>

                <div class="form-group">
                    <label for="date_birthday" class="col-md-4 control-label">Birth date</label>
                    <div class="col-md-6">
                        <input id="date_birthday" type="date" class="form-control" name="date_birthday" required value="{{$user_type->date_birthday}}">
                    </div>
                </div>

                <div class="form-group">
                    <label for="country" class="col-md-4 control-label">Country</label>
                    <div class="col-md-6">
                        <input id="country" type="text" class="form-control" name="country" required value="{{$user_type->country}}">
                    </div>
                </div>

                <div class="form-group">
                    <label for="city" class="col-md-4 control-label">City</label>
                    <div class="col-md-6">
                        <input id="city" type="text" class="form-control" name="city" required value="{{$user_type->city}}">
                    </div>
                </div>

                <div class="form-group">
                    <label for="address" class="col-md-4 control-label">Address</label>
                    <div class="col-md-6">
                        <input id="address" type="text" class="form-control" name="address"  value="{{$user_type->address}}">
                    </div>
                </div>

                <div class="form-group">
                    <label for="tel" class="col-md-4 control-label">Tel</label>
                    <div class="col-md-6">
                        <input id="tel" type="text" class="form-control" name="tel" required value="{{$user_type->tel}}">
                    </div>
                </div>

                <div class="form-group">
                    <label for="spe" class="col-md-4 control-label">Specification</label>
                    <div class="col-md-6">
                        <select class="selectpicker" name="spe" id="spe">
                            <option @if($user_type->spe == " Web") selected="" @endif id="Web" name="Web" value="1"> Web</option>
                            <option @if($user_type->spe == " Compilé") selected="" @endif id="Compile" name="Compile" value="2"> Compilé</option>
                            <option @if($user_type->spe == " Réseau") selected="" @endif id="Rsx" name="Rsx" value="3"> Réseau</option>
                            <option @if($user_type->spe == "Multi Compétences") selected="" @endif id="multi" name="multi" value="4">Multi Compétences</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="comment" class="col-md-4 control-label">Comment</label>
                    <div class="col-md-6">
                        <textarea id="comment" type="text" class="form-control" name="comment" >{{$user_type->comment}}</textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Register
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>