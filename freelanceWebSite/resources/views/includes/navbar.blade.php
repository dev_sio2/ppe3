    <nav class="navbar navbar-inverse navbar-static-top m-b-0 ">
        <div class="navbar-header"> <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="ti-menu"></i></a>
            <div style="height: 60px; width: 200px; background-color: lightgray;" class="top-left-part">
                <a class="logo" href="{{url('/')}}"><img src="{{url('/plugins/images/leLogo.png')}}" style="height: 90px; width: 200px;" ></a>
            </div>

            <ul class="nav navbar-top-links navbar-left hidden-xs">
                <li><a href={{url('/freelance')}}>Freelances</a></li>
                <li><a href={{url('/society')}}>Societies</a></li>
                @if (Auth::guest() || Auth::user()->type == "1")
                <li><a href={{url('/list_project')}}>Project</a></li>
                @else
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Projet <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href={{url('/create_project')}}>Create</a></li>
                            <li><a href={{url('/list_project')}}>List</a></li>
                        </ul>
                </li>
                @endif
            </ul>

            <ul class="nav navbar-top-links navbar-right pull-right">
                <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ url('/login') }}">Login</a></li>
                        <li><a href="{{ url('/register') }}">Register</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{url('/my_profile/')}}/{{Auth::user()->id}}">My account</a></li>
                                <li><a href={{url('/my_project')}}>My projects</a></li>
                                <li>
                                    <a href="{{ url('/logout') }}"
                                       onclick="event.preventDefault();
                                       document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>
                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    @endif
            </ul>

        </div>


    </nav>
