<div class="modal fade" id="applyProject" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Are you sure you want to apply for this project ?</h4>
            </div>
            <div class="modal-footer">
                <a href="{{ url('/my_project/postul/')}}/{{$project->id}}"> <button id="btnSave" class="btn btn-rounded btn-success">Valider</button></a>
                <button type="button" class="btn btn-rounded btn-default" data-dismiss="modal">Fermer</button>
            </div>
        </div>

    </div>
</div>