@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Create Project</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/create_project') }}">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                            <label for="price" class="col-md-4 control-label">Price</label>

                            <div class="col-md-6">
                                <input id="price" type="number" class="form-control" name="price" value="{{ old('price') }}" required>

                                @if ($errors->has('price'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('price') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            <label for="description" class="col-md-4 control-label">Description</label>


                            <div class="col-md-6">
                                <textarea id="description" type="text-area" class="form-control" name="description" required></textarea>

                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

{{-- 
                        <div class="form-group{{ $errors->has('statut') ? ' has-error' : '' }}">
                            <label for="statut" class="col-md-4 control-label">Statut</label>

                            <div class="col-md-6">
                                <input id="statut" type="text" class="form-control" name="statut" required>

                                @if ($errors->has('statut'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('statut') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div> --}}

                         <div class="form-group{{ $errors->has('spe') ? ' has-error' : '' }}">
                            <label for="spe" class="col-md-4 control-label">Specification</label>

                            <div class="col-md-6">
                                <input id="spe" type="text" class="form-control" name="spe" required>


                                @if ($errors->has('spe'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('spe') }}</strong>

                                    </span>
                                @endif
                            </div>
                        </div>

                      <div class="form-group{{ $errors->has('dt_start') ? ' has-error' : '' }}">
                            <label for="dt_start" class="col-md-4 control-label">Date Start</label>

                            <div class="col-md-6">
                                <input id="dt_start" type="date" class="form-control" name="dt_start" required>


                                @if ($errors->has('dt_start'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('dt_start') }}</strong>

                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('dt_end') ? ' has-error' : '' }}">
                            <label for="dt_end" class="col-md-4 control-label">Date End</label>

                            <div class="col-md-6">
                                <input id="dt_end" type="date" class="form-control" name="dt_end" required>


                                @if ($errors->has('dt_end'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('dt_end') }}</strong>

                                    </span>
                                @endif
                            </div>
                        </div> 

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection