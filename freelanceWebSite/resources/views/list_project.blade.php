@extends('layouts.app')

@section ('content')
    <div class="row">
        <h1 class="page-title">Project</h1>
        <div class="white-box col-sm-12 col-md-10 col-md-offset-1">
            <p>Vous trouverez ci-dessous tous les projets proposés par toutes nos entreprises partenaires :</p><br>
            <table id="tblProject" class="datatable" >
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Speciality</th>
                    <th>Price</th>
                    <th>Society</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($projects as $project)
 				<tr  url="{{url('/show_p/')}}/{{$project->id}}" class="line hover-tab">
                    <td>{{ $project->name }}</td>
                    <td>{{ $project->spe }}</td>
                    <td>{{ $project->price }} $</td>
                    <td>{{ App\Society::find($project->society_id)->name }}</td>
                </tr>
				@endforeach
               
                </tbody>
            </table>
        </div>
    </div>

<script type="text/javascript">
$( document ).ready(function() {
    $(".line").click(function(){
        document.location.href= $(this).attr("url");
    });
});
</script>
@endsection