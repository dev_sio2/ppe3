@extends ('layouts.app')

@section ('content')
<div class="row">
 <div class="col-sm-12 col-md-10 col-md-offset-1">
     <div class="block">

   <h1>Profil of : {{strtoupper($profil->name)}}</h1>
    <ul>
     <li><p>Speciality : {{$profil->spe}}</p></li>
     <li><p>Address : {{$profil->address}}</p></li>
     <li><p>Country : {{$profil->country}}</p></li>
     <li><p>City : {{$profil->city}}</p></li>
     <li><p>Code Postal : {{$profil->code_postal}}</p></li>
     <li><p>Mail : {{$user->email}}</p></li>
     <li><p>Tel : {{$profil->tel}}</p></li>
     <li><p>Comment : {{$profil->comment}}</p></li>
   </ul>
     </div>
   
    <div class="block">
    <h1 class="page-title">Project(s)</h1>

        <p>Vous trouverez ci-dessous tous les projets proposés par : {{strtoupper($profil->name)}}</p><br>
        <table id="tblProject" class="datatable" >
            <thead>
            <tr>
                <th>Name</th>
                <th>Speciality</th>
                <th>Price</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($projects as $project)
    <tr  url="{{url('/show_p/')}}/{{$project->id}}" class="line hover-tab">
                <td>{{ $project->name }}</td>
                <td>{{ $project->spe }}</td>
                <td>{{ $project->price }} $</td>
            </tr>
    @endforeach
           
            </tbody>
        </table>
    </div>
    </div>

 </div>

<script type="text/javascript">
$( document ).ready(function() {
    $(".line").click(function(){
        document.location.href= $(this).attr("url");
    });
});
</script>
 @endsection