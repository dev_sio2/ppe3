@extends ('layouts.app')

@section ('content')
<div class="row">
 <div class="col-sm-12 col-md-10 col-md-offset-1">
  <div class="block">

   <h1>{{$profil->lastname}} {{$profil->firstname}}</h1>
    <ul>
     <li><p>Date Birthday : {{$profil->date_birthday}}</p></li>
     <li><p>Quizz Note : {{$profil->note}}/10</p></li>
     <li><p>Speciality : {{$profil->spe}}</p></li>
     <li><p>Address : {{$profil->address}}</p></li>
     <li><p>Country : {{$profil->country}}</p></li>
     <li><p>City : {{$profil->city}}</p></li>
     <li><p>Mail : {{$user->email}}</p></li>
     <li><p>Tel : {{$profil->tel}}</p></li>
     <li><p>Comment : {{$profil->comment}}</p></li>
    </ul>
  </div>
     </div>
   </div>

   @endsection