@extends('layouts.app')

@section ('content')
    <div class="row">
        <h1 class="page-title">Profils Freelances</h1>
        <div class="white-box col-sm-12 col-md-10 col-md-offset-1">
            <p>Ici sont listés tous les profils de nos Freelanceurs :</p><br>
            <table class="datatable" style="width:100%">
                <thead>
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Speciality</th>
                    <th>Country</th>
                </tr>
                </thead>
                <tbody>
                @foreach($profils as $profil)
                <tr url="{{url('/done/')}}/{{$profil->id}}" class="line hover-tab">
                    <td>{{ $profil->firstname }}</td>
                    <td>{{ $profil->lastname }}</td>
                    <td>{{ $profil->spe }}</td>
                    <td>{{ $profil->city }}</td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <script type="text/javascript">
        $( document ).ready(function() {
            $(".line").click(function(){
                document.location.href= $(this).attr("url");
            });
        });
    </script>
@endsection