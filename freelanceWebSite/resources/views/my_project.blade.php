@extends('layouts.app')

@section ('content')
    <div class="row">
    @if(Auth::user()->type == "1")
        <div class="col-sm-12 col-md-10 col-md-offset-1">
            <h1>My project in wait</h1>

            <table id="tblProject" class="datatable" style="width:100%">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Speciality</th>
                    <th>Price</th>
                    @if (Auth::user()->type == 2)
                    <th>Postul</th>
                    @endif
                    <th>Statut</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($projectsWait as $project)
 				<tr url="{{url('/show_p/')}}/{{$project->id}}"  class="line hover-tab">
                    <td>{{ $project->name }}</td>
                    <td>{{ $project->spe }}</td>
                    <td>{{ $project->price }} $</td>
                    @if (Auth::user()->type == 2)
                    @if ($project->postul == true)
                    <td><button class="btn btn-success"></button></td>
                    @else
                    <td><button class="btn btn-danger"></button></td>
                    @endif
                    @endif
                    <td>{{$project->statut}}</td>
                </tr>
				@endforeach
                </tbody>
            </table>
        </div>

        <div class="col-sm-12 col-md-10 col-md-offset-1">
            <h1>My project valid</h1>

            <table id="tblProject" class="datatable" style="width:100%">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Speciality</th>
                    <th>Price</th>
                    @if (Auth::user()->type == 2)
                    <th>Postul</th>
                    @endif
                    <th>Statut</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($projectsOk as $project)
                <tr url="{{url('/show_p/')}}/{{$project->id}}"  class="line hover-tab">
                    <td>{{ $project->name }}</td>
                    <td>{{ $project->spe }}</td>
                    <td>{{ $project->price }} $</td>
                    @if (Auth::user()->type == 2)
                    @if ($project->postul == true)
                    <td><button class="btn btn-success"></button></td>
                    @else
                    <td><button class="btn btn-danger"></button></td>
                    @endif
                    @endif
                    <td>{{$project->statut}}</td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>

        <div class="col-sm-12 col-md-10 col-md-offset-1">
            <h1>My project delete</h1>

            <table id="tblProject" class="datatable" style="width:100%">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Speciality</th>
                    <th>Price</th>
                    @if (Auth::user()->type == 2)
                    <th>Postul</th>
                    @endif
                    <th>Statut</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($projectsDel as $project)
                <tr url="{{url('/show_p/')}}/{{$project->id}}"  class="line hover-tab">
                    <td>{{ $project->name }}</td>
                    <td>{{ $project->spe }}</td>
                    <td>{{ $project->price }} $</td>
                    @if (Auth::user()->type == 2)
                    @if ($project->postul == true)
                    <td><button class="btn btn-success"></button></td>
                    @else
                    <td><button class="btn btn-danger"></button></td>
                    @endif
                    @endif
                    <td>{{$project->statut}}</td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        @else
        <div class="col-sm-12 col-md-10 col-md-offset-1">
            <h1>My project(s)</h1>

            <table id="tblProject" class="datatable" style="width:100%">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Speciality</th>
                    <th>Price</th>
                    @if (Auth::user()->type == 2)
                    <th>Postul</th>
                    @endif
                    <th>Statut</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($projects as $project)
                <tr url="{{url('/show_p/')}}/{{$project->id}}"  class="line hover-tab">
                    <td>{{ $project->name }}</td>
                    <td>{{ $project->spe }}</td>
                    <td>{{ $project->price }} $</td>
                    @if (Auth::user()->type == 2)
                    @if ($project->postul == true)
                    <td><button class="btn btn-success"></button></td>
                    @else
                    <td><button class="btn btn-danger"></button></td>
                    @endif
                    @endif
                    <td>{{$project->statut}}</td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
@endif
    </div>

<script type="text/javascript">
$( document ).ready(function() {
    $(".line").click(function(){
        document.location.href= $(this).attr("url");
    });
});
</script>

@endsection