@extends ('layouts.app')

@section ('content')
    <div class="row">
        @include('includes.applyProject')
        <div class="block">
            <h2>{{$project->name}}</h2>
            <ul>
                <li><p>Description : {{$project->description}}</p></li>
                <li><p>Price : {{$project->price}} $</p></li>
                <li><p>Speciality : {{$project->spe}}</p></li>
                <li><p>Date End : {{$project->dt_start}}</p></li>
                <li><p>Date Start : {{$project->dt_end}}</p></li>
                <li><p>Society : <a href="{{url('/done_p/')}}/{{App\Society::find($project->society_id)->id}}">{{App\Society::find($project->society_id)->name}}</a></p></li>
                @if(!Auth::guest())
                @if(Auth::user()->type == "2" && $profil->id == $project->society_id && $project->id_freelance == null || Auth::user()->isAdmin == 1)
                    <a href="{{ url('/show_p/update/') }}/{{$project->id}}"><button class="btn  btn-primary">Update Project</button></a>
                    <a href="{{ url('/show_p/destroy/') }}/{{$project->id}}"><button class="btn  btn-danger">Del Project</button></a>
                @endif
                @if(Auth::user()->type == "2" && $profil->id == $project->society_id && $project->id_freelance == !null)
                <a href="{{ url('/show_p/validate/') }}/{{$project->id}}"><button class="btn  btn-success">Validate project finish</button></a>
                @endif
                @if(Auth::user()->type == "1" && $postulate == true)
                    <button data-toggle="modal" data-target="#applyProject" type="button" class="btn  btn-success">
                        Postulate Project
                        <i class="fa fa-check " aria-hidden="true"></i>
                    </button>
                @endif
            </ul>
        </div>
        @if($project->postul != 0 && Auth::user()->type == "2" && $profil->id == $project->society_id )
         <div class="block">
                @foreach ($postuls as $postul)
                <p> Freelance: {{ $postul->firstname }} </p>
                <p> Voir le freelance : <a href="{{ url('/done/')}}/{{$postul->id}}">cliquez-ici</a></p>
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/show_p/')}}/{{$project->id}}/{{$postul->id_postul}}">
                {{ csrf_field() }}
                <button id="valid" name="valid" value="{{$postul->id}}" class="btn btn-rounded btn-success">Valider</button> 
                <button id="refus" name="refus" value="true" class="btn btn-rounded btn-delete">Refuser</button>
                </form>
                @endforeach
        </div> 
            @endif
            @endif
    </div>
@endsection