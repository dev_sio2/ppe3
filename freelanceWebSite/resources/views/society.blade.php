@extends('layouts.app')

@section ('content')
    <div class="row">
    <h1 class="page-title">Societies</h1>
        <div class="white-box col-sm-12 col-md-10 col-md-offset-1">
            <p>Ici vous pouvez consulter la liste de nos entreprises partenaires. N'hésitez pas à entrer en contact avec !</p><br>
            <table class="datatable">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Speciality</th>
                    <th>Country</th>
                </tr>
                </thead>
                <tbody>
                @foreach($profils as $profil)
                <tr url="{{url('/done_p/')}}/{{$profil->id}}" class="line hover-tab" >
                    <td>{{ $profil->name }}</td>
                    <td>{{ $profil->spe }}</td>
                    <td>{{ $profil->country }}</td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <script type="text/javascript">
        $( document ).ready(function() {
            $(".line").click(function(){
                document.location.href= $(this).attr("url");
            });
        });
    </script>
@endsection