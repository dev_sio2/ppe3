@extends('layouts.app')
@section ('content')
    <div class="row">
    <div id="Carousel" class="carousel slide col-lg-12 col-offset-2" >
        <ol class="carousel-indicators">
            <li data-target="Carousel" data-slide-to="0" class="active"></li>
            <li data-target="Carousel" data-slide-to="1"></li>
            <li data-target="Carousel" data-slide-to="2"></li>
        </ol>

        <div class="carousel-inner">
            <div class="item active">
                <img style="height: 400px; width: 100%;" src="../public/plugins/images/slide1.jpg" class="img-responsive">
            </div>
           <div class="item">
             <img style="height: 400px; width: 100%;" src="../public/plugins/images/slide2.jpg" class="img-responsive">
            </div>
           <div class="item">
             <img style="height: 400px; width: 100%;" src="../public/plugins/images/slide3.jpg" class="img-responsive">
            </div>
        </div>

        <a class="left carousel-control" href="#Carousel" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
        </a>
        <a class="right carousel-control" href="#Carousel" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
</div>
        <div class="span8">

<p>Retrouvez <strong>plus de 520 Freelances .NET, ASP, C#, ...</strong> inscrits sur notre plateforme avec des compétences variées pour répondre à toutes les demandes de votre entreprise.<br><br>

 <strong>Retrouvez parmi les spécialistes de cette prestation web :</strong><br>
 </p><ul>
  <li>Des développeurs web spécialisés dans la conception d'un site web complexe sous ASP.NET afin de profiter des différentes possibilités de VB.NET et C#, </li>
  <li>Des programmeurs experts dans l'installation et la configuration d'un serveur IIS et du framework .NET pour l'hébergement d'un site web ASP.NET,</li>
  <li>Des développeurs spécialisés dans la conception d'une application web aux fonctionnalités riches sous ASP.NET,</li>
  <li>Des développeurs qui proposent des prestation de correction de bugs afin d'assurer la stabilité d'un site ou d'une application ASP.NET,</li>
  <li>Des spécialistes des ERP et CRM sous ASP.NET et développé en C# pour offrir un outil performant, notamment pour les grandes entreprises,</li>
    <li>Des designers web qui se spécialisent dans la conception d'une interface graphique pour un site sous ASP.NET/C#,</li>
    <li>Des programmeurs sous C qui proposent le développement d'une application .NET utilisable hors ligne,</li>
    <li>Des formateurs en vue d'un perfectionnement dans le développement .NET, ASP, C#
.</li>
 </ul>

<p>Déposez un projet sur Codeur.com afin de recevoir gratuitement plusieurs devis pour la réalisation d'un projet de développement .NET, ASP, C#, ou des contacts si vous recherchez un spécialiste à embaucher. Une fois votre projet déposé, il sera accessible à tous nos freelances. Ainsi, vous serez visible auprès des 520 freelances  inscrits. Vous recevrez rapidement des devis et des contacts.</p>
<br>
</div>
    </div>


<!--jquery knob -->
<script src="plugins/bower_components/knob/jquery.knob.js"></script>
<script>
    $( document ).ready(function() {
        $('[data-plugin="knob"]').knob();
        $('.carousel').carousel()
    });
</script>
   @endsection