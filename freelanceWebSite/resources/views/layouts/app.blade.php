<html>
<head>
    <title>FreelanceWebSite - @yield('title')</title>

    <link rel="stylesheet" href="{{ url('/css/lib.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ url('/css/main.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ url('/css/default-dark.css') }}" type="text/css"/>

    <style>
        .title-block{
            display: inline-block;
        }
        #wrapper{
                margin-bottom: 60px;
        }
        .block{
            border-style: double;
            margin: 10px 40% 10px 13%;
            padding: 10px 10px 10px 10%;
        }
        .hover-tab:hover{
            background-color: #3d6983;
            cursor: pointer;
        }
        footer{
                position: absolute;
            left: 0px;
            right: 0px;
            bottom: 0px;
        }
        .footer-bottom {
            background: #E3E3E3;
            border-top: 1px solid #DDDDDD;
            padding-top: 10px;
            padding-bottom: 10px;
        }
        .footer-bottom p.pull-left {
            padding-top: 6px;
        }
    </style>
</head>
<body>


<script src="{{ url('/js/all.js') }}" ></script>

<div id="wrapper" >
    @include('includes.navbar')
    <div id="">
        <div class="container-fluid">
            @yield('content')
        </div>
    </div>
</div>
@include('includes.footer')
<script>
    $( document ).ready(function() {
        $('.dataTable').DataTable({
            "language": {
                "url" : "/w/js/datatable_i18n/fr_FR.json"
            },
        });
    });
</script>
</body>


</html>