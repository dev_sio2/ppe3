@extends('layouts.app')

@section ('content')
    <div class="row">
        <div class="col-sm-12 col-md-10 col-md-offset-1">
            <h1>Test de connaissance</h1>
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/quizz/update')  }}">
			<input type="hidden" name="idSpe" value="{{$idSpe}}">
            {{ csrf_field() }}
            @foreach ($quizz as $question)
		    		<div class="col-xs-3">
                        <h3>{{$question->name}}</h3>
		                <p>{{$question->question}}</p>
		                @foreach ($responses as $rep)
		                	@if ($rep->id_question == $question->id)
			                	<div>
			                		<label>{{$rep->response}}</label>
			                		<input type="checkbox" value="{{$question->id."-".$rep->id}}" name="{{$question->id}}">
			                	</div>
			                @endif
		                @endforeach
		            </div>
			@endforeach
			<div class="col-md-8 col-md-offset-4">
	            <button type="submit" class="btn btn-primary">
	                Submit
	            </button>
	        </div>
			</form>
        </div>
    </div>
@endsection