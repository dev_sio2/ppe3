@extends ('layouts.app')

@section ('content')
    <div class="row">
        <div class="block">
            <h2>Paiement du Freelance</h2>
            <h4>Si vous estimez le projet terminé comme convenu avec le Freelance, merci de régler le montant du ici :</h4>

            <form class="form-horizontal" role="form" method="POST" action="{{url('paiement').'/'.$id}}">
                {{ csrf_field() }}
                <label for="nb_cart">Entrez votre numéro de carte</label>
                <input type="text" name="nb_cart" placeholder="Numéro de carte">
                <button id="valid" name="valid" value="" class="btn btn-rounded btn-success">Valider</button>
            </form>
        </div>
    </div>
@endsection