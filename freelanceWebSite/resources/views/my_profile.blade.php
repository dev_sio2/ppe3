@extends ('layouts.app')

@section ('content')
<div class="postul cover-flow" hidden>
</div>
<div class="postul popUp-confirm" hidden>
    <h3>Are you sure you want to delet your profil ?</h3>
     <a href="{{ url('/my_profile/destroy/') }}*3"><button class="hidePostul btn btn-success">Yes</button></a>
    <button class="hidePostul btn btn-danger">No</button>
</div>
<div class="row"> 
 <div class="col-sm-12 col-md-10 col-md-offset-1">
     <div class="block">
       @if(Auth::user()->type == "1")
            <div class="title-block user-profile">
                <div class="user-pro-body">
                     @if($profil->file_id  > 0)
                        <img src="{{ url('../storage/app/') }}/{{$profil->file->filename}}" class="img-circle" style="width:70px">
                    @else
                        <img src="{{url('/plugins/images/user.png')}}" alt="user-img" class="img-circle">
                    @endif
                </div>
            </div>
            <h1 class="title">{{$profil->lastname}} {{$profil->firstname}}</h1>
        <ul>
         <li><p>Date Birthday : {{$profil->date_birthday}}</p></li>
         <li><p>Speciality : {{$profil->spe}}</p></li>  
         <li><p>Address : {{$profil->address}}</p></li>
         <li><p>Country : {{$profil->country}}</p></li>
         <li><p>City : {{$profil->city}}</p></li>
         <li><p>Mail :  {{Auth::user()->email}}</p></li>
         <li><p>Tel : {{$profil->tel}}</p></li>
         <li><p>Comment : {{$profil->comment}}</p></li>
         <li><p>Note QCM : {{$profil->note}}/10</p></li>
        </ul>
       @else
            <div class="title-block user-profile">
                <div class="user-pro-body">
                      @if($profil->file_id > 0)
                        <img src="{{ url('../storage/app/') }}/{{$profil->file->filename}}" class="img-circle" style="width:70px">
                    @else
                        <img src="{{url('/plugins/images/user.png')}}" alt="user-img" class="img-circle">
                    @endif
                </div>
            </div>
            <h1 class="title-block">{{$profil->name}} </h1>
            <div>
            <ul>
             <li><p>Date Birthday : {{$profil->date_birthday}}</p></li>
             <li><p>Speciality : {{$profil->spe}}</p></li>
             <li><p>Address : {{$profil->address}}</p></li>
             <li><p>Country : {{$profil->country}}</p></li>
             <li><p>City : {{$profil->city}}</p></li>
             <li><p>Code Postal : {{$profil->ccode_postality}}</p></li>
             <li><p>Mail : {{Auth::user()->email}}</p></li>
             <li><p>Tel : {{$profil->tel}}</p></li>
             <li><p>Speciality : {{$profil->spe}}</p></li>
             <li><p>Commentaire : {{$profil->comment}}</p></li>
            </ul>
        @endif
      <a href="{{ url('/modify_profile/edit/')}}"><button class="btn btn-primary">Update Profil</button></a>
     <button id="delet" class="btn btn-danger">Del Profil</button>
    </div>
     </div>
    </div>

<script type="text/javascript">
$( document ).ready(function() {
    $("#delet").click(function(){
        $(".postul").show();
    });
    $(".hidePostul").click(function(){
        $(".postul").hide();
    });
});
</script>
@endsection