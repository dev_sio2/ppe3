/**
 * Created by joebonnet on 14/12/16.
 */
$(document).ready(function(){
    $('#tblFreelance').DataTable({
        "language": {
            "url" : "./js/datatable_i18n/fr_FR.json"
        },
        "order": [[0, 'desc']],
    });
    $('#tblSociety').DataTable({
        "language": {
            "url" : "./js/datatable_i18n/fr_FR.json"
        },
        "order": [[0, 'desc']],
    });
    $('#tblProject').DataTable({
        "language": {
            "url" : "./js/datatable_i18n/fr_FR.json"
        },
        "order": [[0, 'desc']],
    });


});