const elixir = require('laravel-elixir');
var gulp = require('gulp');

require('laravel-elixir-vue-2');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */
elixir(function(mix){

    gulp.src(['resources/assets/font-awesome/fonts/**/*']).pipe(gulp.dest('public/fonts/'));

    mix.less(
        [
            'node_modules/bootstrap/less/bootstrap.less',
            'node_modules/elite-theme/eliteadmin-dark/less/style.less',
            'resources/assets/less/custom-style.less',
        ],
        'public/css/main.css', './');
    mix.less('node_modules/elite-theme/eliteadmin-dark/less/colors/default-dark.less', 'public/css/default-dark.css', './');
    mix.styles([
        'node_modules/bootstrap/dist/css/bootstrap.min.css',
        'node_modules/elite-theme/eliteadmin-dark/css/animate.css',
        'node_modules/elite-theme/custom_modules/jquery.customSelect/jquery.customSelect.css',
        'node_modules/jquery-toast-plugin/dist/jquery.toast.min.css',
        'node_modules/elite-theme/eliteadmin-dark/plugins/datatable/css/datatable.css'
    ], 'public/css/lib.css', './');

    mix.copy('node_modules/elite-theme/eliteadmin-dark/less/spinners.css', 'public/build/css/');
    mix.copy('node_modules/elite-theme/eliteadmin-dark/less/icons/font-awesome/fonts/', 'public/build/less/icons/font-awesome/fonts');
    mix.copy('node_modules/elite-theme/eliteadmin-dark/less/icons/themify-icons/fonts/', 'public/build/less/icons/themify-icons/fonts');
    mix.copy('node_modules/elite-theme/eliteadmin-dark/less/icons/simple-line-icons/fonts/', 'public/build/less/icons/simple-line-icons/fonts');
    mix.copy('node_modules/elite-theme/eliteadmin-dark/less/icoguns/weather-icons/font/', 'public/build/less/icons/weather-icons/font');
    mix.copy('node_modules/elite-theme/eliteadmin-dark/less/icons/linea-icons/fonts/', 'public/build/less/icons/linea-icons/fonts');
    mix.copy('node_modules/elite-theme/custom_modules/jquery.customSelect/select2.png', 'public/css');
    mix.copy('node_modules/elite-theme/custom_modules/jquery.customSelect/custom-select-spinner.gif', 'public/css/select2-spinner.gif');
    mix.copy('node_modules/elite-theme/custom_modules/jquery.customSelect/select22x2.png', 'public/css');
    mix.copy('node_modules/elite-theme/eliteadmin-dark/plugins/datatable/lang/french.json', 'public/lang/datatable/');
    mix.copy('node_modules/elite-theme/eliteadmin-dark/fonts/stellicons', 'public/build/fonts')

    mix.scripts([
        'node_modules/jquery/dist/jquery.min.js',
        'node_modules/jquery-toast-plugin/dist/jquery.toast.min.js',
        'node_modules/bootstrap/dist/js/bootstrap.min.js',
        'node_modules/elite-theme/custom_modules/jquery.customSelect/jquery.customSelect.js',
        'node_modules/elite-theme/eliteadmin-dark/js/jquery.slimscroll.js',
        'node_modules/elite-theme/eliteadmin-dark/js/waves.js',
        'node_modules/elite-theme/eliteadmin-dark/js/custom.min.js',
        'node_modules/datatables/media/js/jquery.dataTables.min.js',
        'node_modules/datatables-buttons/js/dataTables.buttons.js',
        'node_modules/elite-theme/custom_modules/jquery.customSelect/jquery.customSelect.min.js'
    ], 'public/js/all.js', './');

    mix.version([
        'js/all.js',
        'css/main.css',
        'css/lib.css',
        'css/default-dark.css',
    ]);

});

