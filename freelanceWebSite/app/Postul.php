<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Postul extends Model
{

	public function postul()
	{
		return $this->belongsToMany('App\Project');
	}
    protected $fillable = ['id_freelance', 'id_project', 'statut'];
    protected $primaryKey = 'id_postul'; 
}
