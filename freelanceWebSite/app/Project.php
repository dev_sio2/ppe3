<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Project extends Model
{
	use Notifiable;

	//Methode qui permet de faire les relation entre les table concernées
	public function project()
	{
		return $this->belongsTo('App\Society');
	}

	public function postulP()
	{
		return $this->belongsToMany('App\Postul');
	}
	
   protected $fillable = ['name', 'spe', 'society_id', 'id_freelance', 'postul', 'statut', 'description', 'price', 'dt_start', 'dt_end' , ];
}
