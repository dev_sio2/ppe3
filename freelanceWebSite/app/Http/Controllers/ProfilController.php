<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Freelance;
use App\Society;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use Illuminate\Support\Facades\Session;
use App\User;
use Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class profilController extends Controller
{
	//Methode qui affiche les information du profil d'un freelance ou d'une société
	public function informationProfile($id){
		if (Auth::user()->type == "1") {
			$profil = Freelance::where('user_id',$id)->first();
		}else{	
			$profil = Society::where('user_id',$id)->first();
		}
		return view('my_profile', compact('profil'));
	}

	//Methode qui supprime le profil du freelance
	public function destroy($id)
	{
		User::where('id', $id)->update(['type' => 0]);

		$id_user = Auth::id();

		Session::flush();

		return redirect('/');
	}

	//Methode qui affiche les info du freelance ou de la société afin de modifier son profil
	public function edit(Request $request){
		$id_user = Auth::user()->id;
		$user = User::findOrFail($id_user);

		if ($user->type == 1) {
			$user_type = Freelance::where('user_id',$id_user)->first();

		}
		elseif ($user->type == 2) {
			$user_type = Society::where('user_id',$id_user)->first();
		
		}
		return view('modify_profil', compact('user', 'user_type'));
	}

	//Methode qui permet d'update les info du profil apres les avoirs modifiées
	public function update($type, Request $request)
	{
		$id = Auth::id();
		$user_type = Freelance::where('user_id',$id)->first();

		$new_user = User::where('id', $id)->update([
            'name' => $request->input('name'),
            'email' => $request->input('email')
        ]);

		
		if($request->file('file') == null){
		
		
        if ($type == 1) {
        $user = Freelance::where('user_id', $id)->update([
            'firstname' => $request->input('firstname'),
            'lastname' => $request->input('lastname'),
            'date_birthday' => $request->input('date_birthday'),
            'country' => $request->input('country'),
            'city' => $request->input('city'),
            'address' => $request->input('address'),
            'spe' => $request->input('spe'),
            'tel' => $request->input('tel'),
            'comment' => $request->input('comment'),
            'file_id' => $request->input('hidden'),
            ]);
        }elseif ($type == 2){
            $user = Society::where('user_id', $id)->update([
            'name' => $request->input('name'),
            'mail' => $request->input('mail'),
            'country' => $request->input('country'),
            'city' => $request->input('city'),
            'address' => $request->input('address'),
            'spe' => $request->input('spe'),
            'tel' => $request->input('tel'),
            'comment' => $request->input('comment'),
            'code_postal' => $request->input('code_postal'),
            'file_id' => $request->input('hidden'),
            ]);
		}

	}else {

		$file = $request->file('file');
	        $extension = $file->getClientOriginalExtension();
	        Storage::disk('local')->put($file->getFilename().'.'.$extension,  File::get($file));
	 
	        $entry = new \App\File();
	        $entry->mime = $file->getClientMimeType();
	        $entry->original_filename = $file->getClientOriginalName();
	        $entry->filename = $file->getFilename().'.'.$extension;

	        $entry->save();

	        

		  if ($type == 1) {
        $user = Freelance::where('user_id', $id)->update([
            'firstname' => $request->input('firstname'),
            'lastname' => $request->input('lastname'),
            'date_birthday' => $request->input('date_birthday'),
            'country' => $request->input('country'),
            'city' => $request->input('city'),
            'address' => $request->input('address'),
            'spe' => $request->input('spe'),
            'tel' => $request->input('tel'),
            'comment' => $request->input('comment'),
            'file_id' => $entry->id,
            ]);
        }elseif ($type == 2){
            $user = Society::where('user_id', $id)->update([
            'name' => $request->input('name'),
            'mail' => $request->input('mail'),
            'country' => $request->input('country'),
            'city' => $request->input('city'),
            'address' => $request->input('address'),
            'spe' => $request->input('spe'),
            'tel' => $request->input('tel'),
            'comment' => $request->input('comment'),
            'code_postal' => $request->input('code_postal'),
            'file_id' => $entry->id,
            ]);
		}
	}
	return redirect('my_profile/'.$id);
	}

	//Methode qui permet de recuperer le mdp du freelance ou de la société pour pouvoir en suite le modifier avec la fonction ci-dessous update
	public function editPasswd(Request $request){
		if ($request->input('password') == $request->input('password_confirmation')) {
			User::where('id',Auth::id())->update(['password'=>Hash::make($request->input('password'))]);
		}
		return redirect('/modify_profile/edit');
	}

	public function updatePasswd($type, Request $request){	
		return redirect('/modify_profile/edit');
	}

	
}
