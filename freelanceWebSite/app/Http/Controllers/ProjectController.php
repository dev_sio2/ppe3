<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use App\Society;
use App\Http\Requests;
use Auth;
use App\User;
use App\Freelance;
use App\Postul;

class ProjectController extends Controller
{
	//Methode qui permetde return la view pour pouvoir créer un projet
	public function createProject()
	{
		return view('create_project');
	}

	//Methode qui permet de créer un projet en le reliant a la société qui la créé
	public function store(Request $request)
	{
		$this->validate($request, [

			'name' => 'required|max:255',
			'spe' => 'required|max:255',
			'description' => 'required|max:500',
			'price' => 'required|max:255',
			'dt_start' => 'required|max:255',
			'dt_end' => 'required|max:255',
			]);
		$user = Auth::id();	

		$society = Society::where('user_id', '=', $user)->firstOrFail()->project_society()->create([
			'name' => $request->input('name'),
			'spe' => $request->input('spe'),
			'description' => $request->input('description'),
			'price' => $request->input('price'),
			'statut' => "En attente",
			'dt_start' => $request->input('dt_start'),
			'dt_end' => $request->input('dt_end'),

			]);

		return redirect('list_project');
	}

	//Methode qui permet d'afficher les projets
	public function show(Request $request)
	{
		$projects = Project::where('statut', '=', 'En attente')->get();

		return view('list_project', compact('projects'));
	}

	//Methode qui permet d'afficher les information en relation avec le projet selectionné tout en ajoutant certaines fonctionnalité en fonction de la personne qui est connecté
	public function information(Request $request, $id, $id_postul = null)
	{
		$profil = null;
		$project = Project::findOrFail($id);
		if (Auth::guest()) {
			return view('show_p', compact('project','profil'));			
		}
		else{
			$usr = Auth::user();

			if ($usr->type == "1") {
				$profil = Freelance::where('user_id', '=', $usr->id)->first();
				$isPostul = Postul::where('id_freelance', $profil->id)->where('id_project', $id)->first();
				$isValid = Project::where('id_freelance', $profil->id)->where('id', $id)->first();
				if (!$isValid && !$isPostul) {
					$postulate = true;
				}else {
					$postulate = false;
				}
			}
			elseif ($usr->type == "2") {
				if ($request->input('valid')){
					$project->id_freelance = $request->input('valid');
					$project->statut = "Accepté";
				 	$project->save();

				 	$postul = Postul::find($id_postul);
					$postul->statut = "Accepté";
					$postul->save();

					$freelance = Freelance::findOrfail($postul->id_freelance);

					$to      = User::findOrfail($freelance->id)->email;
					$subject = 'Your postulation  : '.$project->name;
					$message = $freelance->firstname.', your application has been accepted for the project '.$project->name.', consult our webSite for more informations';
					$headers = 'From: Freelance@WebSite.fr' . "\r\n" .
					    'Reply-To: Freelance@WebSite.fr' . "\r\n" .
					    'X-Mailer: PHP/' . phpversion();

					mail($to, $subject, $message, $headers);
				}
				elseif($request->input('refus')){
					$postul = Postul::find($id_postul);
					$postul->statut = "Refusé";
					$postul->save();

					$freelance = Freelance::findOrfail($postul->id_freelance);

					$onePostul  = Postul::where('id_project', $id)->first();
					if (!$onePostul) {
						$project->postul = 0;
					}

					$to      = User::findOrfail($freelance->id)->email;
					$subject = 'Your postulation  : '.$project->name;
					$message = $freelance->firstname.', sorry your application has been deleted for the project '.$project->name.', consult our webSite for more informations';
					$headers = 'From: Freelance@WebSite.fr' . "\r\n" .
					    'Reply-To: Freelance@WebSite.fr' . "\r\n" .
					    'X-Mailer: PHP/' . phpversion();

					mail($to, $subject, $message, $headers);
				}

				$profil = Society::where('user_id', '=', $usr->id)->first();
				$postuls  = Postul::join('freelances', 'postuls.id_freelance', '=', 'freelances.id')->where('postuls.id_project', '=', $project->id)->where('postuls.statut', 'En attente')->get();
			}
				return view('show_p', compact('project', 'profil', 'postuls', 'postulate'));
		}

		
		
		
	}

	//Methode qui permet de supprimé le projet
	public function destroy($id)
	{
		Project::destroy($id);
		return redirect('list_project');
	}

	//Methode qui permet d'afficher les info du projet pour les modifier
	public function edit($id)
	{ 
		$project = Project::findOrfail($id);
		return view('edit_project', compact('project'));
	}

	//Methode qui permet d'update les information du projet
	public function update(Request $request, $id)
	{
		$project = Project::findOrfail($id);

		$this->validate($request, [

			'name' => 'required|max:255',
			'spe' => 'required|max:255',
			'description' => 'required|max:500',
			'price' => 'required|max:255',
			'dt_start' => 'required|max:255',
			'dt_end' => 'required|max:255',
			]);

		$input = $request->all();
		

		$project->fill($input)->save();

		return redirect('list_project');	
	}

	//Method qui permet d'afficher les projet dans l'option (petit menu deroulant) "my project" du freelance ou de la société
	public function myProject()
	{
		$usr = Auth::user();
		if ($usr->type == "1" ){
			$freelance = Freelance::where('user_id', '=', $usr->id)->firstOrFail();
			$projectsWait = Postul::join('projects', 'postuls.id_project', '=', 'projects.id')->where('postuls.id_freelance', '=', $freelance->id)->where('postuls.statut', 'En attente')->get();

			$projectsOk = Postul::join('projects', 'postuls.id_project', '=', 'projects.id')->where('postuls.id_freelance', '=', $freelance->id)->where('postuls.statut', 'Accepté')->orWhere('postuls.statut', 'Payé')->get();

			$projectsDel = Postul::join('projects', 'postuls.id_project', '=', 'projects.id')->where('postuls.id_freelance', '=', $freelance->id)->where('postuls.statut', 'Refusé')->get();

			return view('my_project', compact('projectsWait', 'projectsOk', 'projectsDel'));
			
		}
		elseif ($usr->type == "2" ){
			$society = Society::where('user_id', '=', $usr->id)->firstOrFail();
			$projects = Project::where('society_id', '=', $society->id)->get();
			return view('my_project', compact('projects'));
		}
	}

	//Methode qui permet de postulé a un projet
	public function postul($id)
	{	
		$freelance = Freelance::where('user_id', '=', Auth::id())->firstOrFail();
		$project = Project::findOrfail($id);
		$society = Society::findOrfail($project->society_id);
		
		$postul = Postul::create([
			'id_freelance' => $freelance->id,	
			'id_project' => $id,
			'statut' => "En attente",
			]);	

		$project->postul = 1;
		$project->save();

		$to      = User::findOrfail($society->id)->email;
		$subject = 'New postul on you project : '.$project->name;
		$message = $society->name.', You are a new postulation on your project '.$project->name.' of the freelance '. $freelance->firstName.' '. $freelance->lastName;
		$headers = 'From: Freelance@WebSite.fr' . "\r\n" .
		    'Reply-To: Freelance@WebSite.fr' . "\r\n" .
		    'X-Mailer: PHP/' . phpversion();

		mail($to, $subject, $message, $headers);

		return redirect('my_project');
	}

	//Methode qui return la vue necessaire au paiement du freelance
	public function validation($id)
	{
		return view('paiement', compact('id'));
	}

	//Methode du paiement suivi de l'envoi du mail
	public function paiement($id)
	{
		$project = Project::find($id);
		$project->statut = 'Payé';
		$project->save();

		$postul = Postul::where('id_freelance', $project->id_freelance)->where('id_project', $project->id)->first();
		$postulate = Postul::find($postul->id_postul); 
		$postulate->statut = 'Payé';
		$postulate->save();

		$society = Society::findOrfail($project->society_id);
		$freelance = Freelance::findOrfail($project->id_freelance);


		$to      = User::findOrfail($freelance->id)->email;
		$subject = 'Paiement for project : '.$project->name;
		$message = $freelance->firstname.',  You were paid for the project '.$project->name.', consult our webSite for more informations';
		$headers = 'From: Freelance@WebSite.fr' . "\r\n" .
		    'Reply-To: Freelance@WebSite.fr' . "\r\n" .
		    'X-Mailer: PHP/' . phpversion();

		mail($to, $subject, $message, $headers);

		$to      = User::findOrfail($society->id)->email;
		$subject = 'Paiement for project : '.$project->name;
		$message = $society->name.',  You paid for the project '.$project->name.', consult our webSite for more informations';
		$headers = 'From: Freelance@WebSite.fr' . "\r\n" .
		    'Reply-To: Freelance@WebSite.fr' . "\r\n" .
		    'X-Mailer: PHP/' . phpversion();

		mail($to, $subject, $message, $headers);

		return redirect('/');
	}

}
