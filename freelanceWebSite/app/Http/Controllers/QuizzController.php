<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Society;
use App\Quizz;
use App\Freelance;
use App\Response;
use App\Http\Requests;
use Auth;
use App\User;
use App\Test;


class QuizzController extends Controller
{

	//Methode qui permet d'afficher le quizz
	public function show(Request $request)
	{
	   $idUser = Auth::id();
	   
	   
	   $type = User::findOrFail($idUser)->type;
	   if ($type == 1) {
	   		$freelance = Freelance::where('user_id', $idUser)->firstOrFail();
   			$note = $freelance->note;
   			$idSpe = $freelance->spe;

   			$quizz = Quizz::all();
   			$responses = Response::all();
	   		if (!$note) {
	   			return view('quizz', compact('quizz', 'responses', 'idSpe'));
	   		}else{
	   	return redirect('/');
	   }
	   }else{
	   	return redirect('/');
	   }
	   
	}

	//Methode qui permet d'update les reponses du freelance
	public function update(Request $request){
		$idUser = Auth::id();
		$note = 0;
		$input = $request->all();

		foreach ($input as $key => $value) {
			if (is_numeric($key)) {
				$rep = Test::where('id', $key)->firstOrFail()->reponse;
				if ($rep == $value) {
					$note++;
				}
			}
		}

		$note = $note*2;

		Freelance::where('user_id', $idUser)->update(['note' => $note]);

		return redirect('/');
	}
}
