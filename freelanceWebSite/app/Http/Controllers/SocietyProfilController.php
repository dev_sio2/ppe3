<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Society;
use App\Http\Requests;
use App\User;
use App\Project;


class SocietyProfilController extends Controller
{
    public function create()
	{
		return view('p-society');
	}

	public function store(Request $request)
	{
		$this->validate($request, [

			'name' => 'required|max:255',
			'code_postal' => 'required|max:255',
			'mail' => 'required|email|max:255',
			'country' => 'required|max:255',
			'city' => 'required|max:255',
			'address' => 'required|max:255',
			'tel' => 'required|max:255',
			'spe' => 'required|max:255',
			'comment' => 'max:500',


			]);


		$society = Society::create([

			'name' => $request->input('name'),
			'mail' => $request->input('mail'),
			'country' => $request->input('country'),
			'city' => $request->input('city'),
			'address' => $request->input('address'),
			'spe' => $request->input('spe'),
			'tel' => $request->input('tel'),
			'comment' => $request->input('comment'),
			'code_postal' => $request->input('code_postal'),

			]);

		return view('/');

	}
	//Methode qui permet d'afficher les sociétés
	public function show(Request $request)
	{
	   $profils = Society::all();
	   return view('society', compact('profils'));
	}

	//Methode qui return la view necessaire au formulaire d'inscription de la société
	public function register()
    {
        return view("includes.regSociety");
    }

    //Methode qui affiche les information en relation avec la société selectionnée
	public function information($id)
	{
		$profil = Society::findOrFail($id);
		$user = User::findOrfail($profil->user_id);
		$projects = Project::where('society_id', $id)->where('statut', 'En attente')->get();

		return view('done_p', compact('profil','user', 'projects'));
	}
}
