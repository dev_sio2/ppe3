<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use App\Http\Requests;
use App\Freelance;
use App\Society;

use Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    
    /**
     * Get a validator for an incoming registration request.
     
     * @param  array  $dat
     * @return \Illuminate\Contracs\Validation\Validator
     *
     */
    //Methode qui vérifie si les champ requis on bien etait remplis
    public function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    
    //Methode qui permet de s'enregistrer en tant que freelance ou société
    public function register(Request $request)
    {
        $new_user = User::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
            'type' => $request->input('type'),
        ]);
        
        $type = $request->input('type'); 
        $latest_id = $new_user->id;

        $file = $request->file('file');
        $entry = new \App\File();
        $entry->id = 0;
        
        if ($file) {
             $extension = $file->getClientOriginalExtension();
            Storage::disk('local')->put($file->getFilename().'.'.$extension,  File::get($file));
     
            
            $entry->mime = $file->getClientMimeType();
            $entry->original_filename = $file->getClientOriginalName();
            $entry->filename = $file->getFilename().'.'.$extension;
     
            $entry->save();
        }

        if ($type == 1) {
        $user = User::find($latest_id)->freelance_user()->create([
            'firstname' => $request->input('firstname'),
            'lastname' => $request->input('lastname'),
            'date_birthday' => $request->input('date_birthday'),
            'country' => $request->input('country'),
            'city' => $request->input('city'),
            'address' => $request->input('address'),
            'spe' => $request->input('spe'),
            'tel' => $request->input('tel'),
            'comment' => $request->input('comment'),
            'file_id' => $entry->id,
            ]);

        }else
        {
            $user = User::find($latest_id)->society_user()->create([
            
            'name' => $request->input('name'),
            'mail' => $request->input('mail'),
            'country' => $request->input('country'),
            'city' => $request->input('city'),
            'address' => $request->input('address'),
            'spe' => $request->input('spe'),
            'tel' => $request->input('tel'),
            'comment' => $request->input('comment'),
            'code_postal' => $request->input('code_postal'),
            'file_id' => $entry->id,
            ]);

        
        }
        $to      =  $request->input('email');
        $subject = 'Register in FreelanceWebSite';
        $message = $request->input('name').', thanks to register in our website, good navigation.';
        $headers = 'From: Freelance@WebSite.fr' . "\r\n" .
            'Reply-To: Freelance@WebSite.fr' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();

        mail($to, $subject, $message, $headers);

        return redirect('/login');         
    }
 
}
