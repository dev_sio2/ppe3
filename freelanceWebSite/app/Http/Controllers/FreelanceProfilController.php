<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Freelance;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use App\User;

class FreelanceProfilController extends Controller
{
	//Methode qui permet l'affichage des Freelance
	public function show(Request $request)
	{
	   $profils = Freelance::all();
	   return view('freelance', compact('profils'));
	}

	//Methode qui permet l'affichage des information relié au freelance selectionné
	public function information($id)
	{
		$profil = Freelance::findOrFail($id);
		$user = User::findOrfail($profil->user_id);

		return view('done', compact('profil','user'));
	}

	//Methode qui permet de supprimer le freelance 
	public function destroy($id)
	{
		Freelance::destroy($id);
		return redirect('list_project');
	}

	//Methode qui permet d'afficher les info du freelance pour les modifier
	public function edit($id)
	{
		$freelance = Freelance::findOrfail($id);
		return view('edit_project', compact('project'));
	}

	//Methode qui return une vue necessaire au formulaire d'un freelance 
    public function register()
    {
        return view("includes.regFreelance");
    }

    //Methode qui update les informations d'un freelance une fois modifiées
	public function update(Request $request, $id)
	{
		$freelance = Freelance::findOrfail($id);

		$this->validate($request, [

			'firstname' => 'required|max:255',
			'lastname' => 'required|max:255',
			'mail' => 'required|email|max:255',
			'country' => 'required|max:255',
			'city' => 'required|max:255',
			'address' => 'required|max:255',
			'tel' => 'required|max:255',
			'spe' => 'required|max:255',
			'comment' => 'max:500',

			]);

		$input = $request->all();
		

		$freelance->fill($input)->save();

	}


}
