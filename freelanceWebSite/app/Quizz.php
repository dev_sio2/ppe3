<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Quizz extends Model
{
	use Notifiable;

	public $table = "tests";


      protected $fillable = [
        'id', 'name', 'question', 'reponse', 'spe',
    ];
}
