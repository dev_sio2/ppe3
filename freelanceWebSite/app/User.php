<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    
    public function freelance_user()
    {
        return $this->hasOne('App\Freelance');
    }

    public function society_user()
    {
        return $this->hasOne('App\Society');
    }

    

    public function test()
    {
        $id = Auth::user()->type;
        return $id;
    }
    

    protected $fillable = [
        'name', 'email', 'password', 'type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
