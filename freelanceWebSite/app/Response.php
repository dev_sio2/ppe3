<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Response extends Model
{
	use Notifiable;

	public $table = "response";


      protected $fillable = [
        'id_question', 'response', 'isTrue',
    ];
}
