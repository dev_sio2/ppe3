<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Freelance extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    
    //Methode qui permet de faire les relation entre les table concernées
    public function freelance()
    {
        return $this->belongsTo('App\User');
    }

     public function file()
    {
        return $this->belongsTo('App\File');
        
    }
    

    protected $fillable = [
        'firstname', 'lastname', 'date_birthday', 'address', 'country', 'city', 'code_postal', 'tel', 'spe', 'comment', 'file_id',
    ];

    protected $hidden = [
    	'password', 'remember_token'
    ];
}
