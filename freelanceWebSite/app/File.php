<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $table = 'files';

    public function file_freelance()
    {
        return $this->hasOne('App\Society');
    }

    
}
