<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Society extends Model
{
	use Notifiable;
    //Methodes qui permet de faire les relation entre les table concernées
	public function society()
	{
		return $this->belongsTo('App\User');
	}

        public function project_society()
    {
        return $this->hasMany('App\Project');
    }

    public function file()
    {
        return $this->belongsTo('App\File');
        
    }

      protected $fillable = [
        'name', 'address', 'country', 'city', 'code_postal', 'tel', 'spe', 'comment', 'file_id', 'code_postal',
    ];

    protected $hidden = [
    	'password', 'remember_token'
    ];
}
