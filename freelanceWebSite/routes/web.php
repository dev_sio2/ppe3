<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();


Route::get('/home', 'HomeController@index');
Route::post('/home', 'HomeController@index');

//Route Freelances
Route::get('p-freelance', 'FreelanceProfilController@create');
Route::get('/register/freelance', 'FreelanceProfilController@register');
Route::post('p-freelance', 'FreelanceProfilController@store');
Route::get('/freelance', 'FreelanceProfilController@show');
Route::get('/done/{id}', 'FreelanceProfilController@information');

//Route Société
Route::get('p-society', 'SocietyProfilController@create');
Route::post('p-society', 'SocietyProfilController@store');
Route::get('society', 'SocietyProfilController@show'); 
Route::get('done_p/{id}', 'SocietyProfilController@information');
Route::get('/register/society', 'SocietyProfilController@register');

//Route Projets
Route::get('create_project', 'ProjectController@createProject');
Route::post('create_project', 'ProjectController@store');
Route::get('list_project', 'ProjectController@show');
Route::get('/show_p/{id}', 'ProjectController@information');
Route::get('/my_project/postul/{id}', 'ProjectController@postul');
Route::post('/show_p/{id}/{id_postul}', 'ProjectController@information');
Route::post('/paiement/{id}', 'ProjectController@paiement');
Route::get('/show_p/validate/{id}', 'ProjectController@validation');
Route::get('show_p/destroy/{id}', 'ProjectController@destroy');
Route::get('/show_p/update/{id}', 'ProjectController@edit');
Route::post('/show_p/update/p/{id}', 'ProjectController@update');
Route::get('/my_project', 'ProjectController@myProject');
Route::get('/my_project/postul/{id}', 'ProjectController@postul');

//Route Quizz
Route::get('quizz', 'QuizzController@show');
Route::post('quizz/update', 'QuizzController@update');

//Route Profil
Route::get('/my_profile/{id}', 'ProfilController@informationProfile');
Route::get('my_profile/destroy/', 'ProfilController@destroy');
Route::get('/modify_profile/edit', 'ProfilController@edit');
Route::post('/modify_profile/editPasswd', 'ProfilController@editPasswd');
Route::post('/my_profile/update/{type}', 'ProfilController@update');


